import sys
import re
import signal

DEBUG = False

def signal_hander(signal, frame):
    print("\nExiting.", end = "")
    exit(1)

signal.signal(signal.SIGINT, signal_hander) # This is mostly because I don't want to see the traceback on ctrl+c

# filler = lambda word, length : word + " "*(length - len(word)) # Replaced with actual function below. Does same thing

# Returns string with appended trailing spaces to make its length maxlength for nicer printing
def filler(word, maxlength): 
    return word + " "*(maxlength - len(word)) 

# # Turns tabs to spaces. (expandtabs() exists. Also probably doesn't work if tab size is different)
# def tabs_to_spaces(text):
#     return text.replace("\t", "    ")

# Returns dictionary with (key, value) = (unique_words, instances_in_text)
def count_words(filename):

    try: # Make sure file exists 
        text_file = open(filename) 
    except:
        print("File not found, try again")
        return None
    
    word_count = {} # Empty dictionary where the unique words go
    lines = text_file.readlines() # Makes list of entire document divided by line seperators

    if DEBUG:
        print(lines)

    #lines = filter(None, lines) # Should remove any null values. Probably not needed
    text_file.close()

    for line in lines:
        line = line.lower()
        line = line.expandtabs() # Handles words seperated by only tabs
        split_line = line.split(sep = " ")
        for word in split_line:
            if DEBUG:
                print(word)
            word = re.sub(r'[ .,:;!?\n]', "", word) # Remove non-word characters
            if (word == '') or (re.match(r'[\d]', word)): # No empty strings or numbers
                continue
            elif word in word_count:
                word_count[word] += 1 # Increment counter of already seen word
            else:
                word_count[word] = 1 # Add dictionary entry for new unique word
    if not word_count: # Empty file or non-words
        print("File contains no words", end = "")
        exit(1)
    return word_count

def main(argv):

    # Command line argument check
    if len(argv) == 3:
        STATS = True
    elif len(argv) == 1:
        STATS = False
    else:
        print("Error: Bad arguments.\nModes of use: \n  1. Interactive:\t\"wordcount.py\"'\n  2. Print Lists:\t\"wordcount.py [filename] [num_words]\"", end = "")
        exit(2)

    # File from args or user input
    if STATS:
        filename = argv[1]
        word_count = count_words(filename)
        if not word_count:
            print("File not found; Exiting", end = "")
    else:
        word_count = None
        while not word_count:
            filename = input("Enter the name of your text file: ")
            word_count = count_words(filename)
    

    ''' # OF UNIQUE WORD '''
    print("\nThere are", len(word_count), "unique words in", filename, "\n")

    # ''' MODE / MOST USED WORD (Replaced with list of same thing)''' 
    # modenum = max(word_count.values())
    # for word, number in word_count.items():
    #     if number == modenum:
    #         mode = '\'' + word + '\''
    #         break
    # print("The most used word in", filename, "is", mode, "with", modenum, "uses")

    if STATS:
        num_words = int(argv[2])
        if num_words > len(word_count):
            num_words = len(word_count) # Prevent going off the list
        
        WORD = "word" # Interpreter didnt like filler("word", maxlen), so now this string is a constant

        ''' LIST OF MOST USED WORDS '''
        sorted_word_count = sorted(word_count.items(), key = lambda x : x[1], reverse=True)
        # Find max length in range for nicer printing. max() returns tuple of longest first element. len(tuple[0]) gets the length
        maxlen = len(max(sorted_word_count[0:num_words], key = lambda x: len(x[0]))[0])  
        if num_words == int(argv[2]):
            print("The words ordered by greatest usage:")
        else:
            print("The", num_words, "most used words are:")
        header = f"rank \t {filler(WORD, maxlen)} \t count"
        header = header.expandtabs() # Needed to put the correct amount of dashes
        print(header)
        print("-"*len(header))
        for x in range(0,num_words):
            list_num = f"{x+1}."
            print(list_num, '\t', filler(sorted_word_count[x][0], maxlen), '\t', sorted_word_count[x][1])
        print("")

        # ''' LIST OF LEAST USED WORDS (Not very intresting)'''
        # print("The", num_words, "least used words are:")
        # sorted_word_count.reverse()
        # for x in range(0,num_words):
        #     list_num = f"{x+1}."
        #     print(list_num, '\t', sorted_word_count[x][0], '\t', sorted_word_count[x][1])
        # print("")

        ''' LIST OF LONGEST WORDS '''
        sorted_word_count = sorted(word_count.items(), key = lambda k : len(k[0]), reverse = True)
        # Recalculate max length in range for nicer printing, as the list is changed
        maxlen = len(max(sorted_word_count[0:num_words], key = lambda x: len(x[0]))[0])
        if num_words == int(argv[2]):
            print("The words ordered by longest length")
        else:
            print("The", num_words, "longest words are:")
        header = f"rank \t {filler(WORD, maxlen)} \t length \t count"
        header = header.expandtabs() # Needed to put the correct amount of dashes
        print(header)
        print("-"*len(header))

        for x in range(0,num_words):
            list_num = f"{x+1}."
            print(list_num, '\t',filler(sorted_word_count[x][0], maxlen), '\t', len(sorted_word_count[x][0]), '\t',  sorted_word_count[x][1]) #TODO: Get the count column to line up correctly without just double tab
        print("")

    else:
        ''' WORD SEARCH '''
        while True:
            search_term = input("Enter a word to search, or press Enter to quit:")
            lower_search_term = search_term.lower()
            number_found = None
            if search_term == '': # Exit condition
                break
            else:
                for word, number in word_count.items():
                    if word == lower_search_term:
                        number_found = number
                search_term = '\'' + search_term + '\''
                if number_found == None:
                    print("There is no instance of word", search_term, "in", filename)
                else:
                    print ("There are", number_found, "instances of", search_term, "in", filename)
    print("Goodbye", end="")

if __name__ == "__main__":
    main(sys.argv)